package fr.anchorwave.cramezproject.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import fr.anchorwave.cramezproject.model.MyObjectForRecyclerView
import fr.anchorwave.cramezproject.model.ObjectDataHeaderSample
import fr.anchorwave.cramezproject.model.PersonDataUI
import fr.anchorwave.cramezproject.model.RoomPersonData
import fr.anchorwave.cramezproject.repository.PersonRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PersonViewModel : ViewModel() {

    private val personRepository: PersonRepository by lazy { PersonRepository() }

    val personListLiveData: LiveData<List<PersonDataUI>> =
        personRepository.selectAllPerson().map { it.toUi() }

    fun fetchNewPerson() {
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.fetchData()
        }
    }

    fun deleteAllPerson() {
        viewModelScope.launch(Dispatchers.IO) {
            personRepository.deleteAllPerson()
        }
    }
}

private fun List<RoomPersonData>.toUi(): List<PersonDataUI> {
    return asSequence().map {
        PersonDataUI(
            personFirstName = it.first_name,
            personLastName = it.last_name,
            personGender = it.gender,
            personAvatar = it.avatar,
        )
    }.toList()
}

/*private fun List<PersonDataUI>.toMyObjectForRecyclerView(): List<MyObjectForRecyclerView> {
    val result = mutableListOf<MyObjectForRecyclerView>()

    groupBy {
        // Split in 2 list, male or female (+ others)
        it.personGender == "male"
    }.forEach { (isModulo, items) ->
        // For each mean for each list split
        // Here we have a map (key = isModulo) and each key have a list of it's items
        result.add(ObjectDataHeaderSample("Is male: $isModulo"))
        result.addAll(items)
        result.add(ObjectDataHeaderSample("Number of identities :${items.count()}"))
    }
    return result
}*/
