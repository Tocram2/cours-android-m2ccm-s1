package fr.anchorwave.cramezproject.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.anchorwave.cramezproject.model.RoomPersonData

@Dao
interface PersonDao {

    @Query("SELECT * FROM person_object_table ORDER BY last_name ASC")
    fun selectAll(): LiveData<List<RoomPersonData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(person: RoomPersonData)

    @Query("DELETE FROM person_object_table")
    fun deleteAll()
}