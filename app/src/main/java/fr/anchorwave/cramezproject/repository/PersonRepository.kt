package fr.anchorwave.cramezproject.repository

import androidx.lifecycle.LiveData
import fr.anchorwave.cramezproject.architecture.CustomApplication
import fr.anchorwave.cramezproject.architecture.RetrofitBuilder
import fr.anchorwave.cramezproject.model.RoomPersonData
import fr.anchorwave.cramezproject.model.RetrofitPersonData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PersonRepository {
    private val mPersonDao =
        CustomApplication.instance.mApplicationDatabase.personDao()

    fun selectAllPerson(): LiveData<List<RoomPersonData>> {
        return mPersonDao.selectAll()
    }

    private suspend fun insertPerson(personData: RoomPersonData) =
        withContext(Dispatchers.IO) {
            mPersonDao.insert(personData)
        }

    suspend fun deleteAllPerson() = withContext(Dispatchers.IO) {
        mPersonDao.deleteAll()
    }

    suspend fun fetchData() {
        insertPerson(RetrofitBuilder.getPersonData().getRandomPerson().toRoom())
    }

    private fun RetrofitPersonData.toRoom(): RoomPersonData {
        return RoomPersonData(
            first_name = first_name,
            last_name = last_name,
            gender = gender,
            avatar = avatar
        )
    }
}