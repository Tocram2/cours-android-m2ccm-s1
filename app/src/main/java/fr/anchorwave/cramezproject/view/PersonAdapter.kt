package fr.anchorwave.cramezproject.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.anchorwave.cramezproject.R
import fr.anchorwave.cramezproject.databinding.ItemCustomRecyclerBinding
import fr.anchorwave.cramezproject.databinding.ItemCustomRecyclerHeaderBinding
import fr.anchorwave.cramezproject.databinding.ItemCustomRecyclerFooterBinding
import fr.anchorwave.cramezproject.model.MyObjectForRecyclerView
import fr.anchorwave.cramezproject.model.ObjectDataHeaderSample
import fr.anchorwave.cramezproject.model.ObjectDataFooterSample
import fr.anchorwave.cramezproject.model.PersonDataUI
import java.lang.RuntimeException

private val diffItemUtils = object : DiffUtil.ItemCallback<MyObjectForRecyclerView>() {

    override fun areItemsTheSame(
        oldItem: MyObjectForRecyclerView,
        newItem: MyObjectForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: MyObjectForRecyclerView,
        newItem: MyObjectForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }
}

class PersonAdapter(
    private val onItemClick: (quoteUi: PersonDataUI, view: View) -> Unit,
) : ListAdapter<MyObjectForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.ROW.type -> {
                PersonViewHolder(
                    ItemCustomRecyclerBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onItemClick
                )
            }
            MyItemType.HEADER.type -> {
                PersonHeaderViewHolder(
                    ItemCustomRecyclerHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            MyItemType.FOOTER.type -> {
                PersonFooterViewHolder(
                    ItemCustomRecyclerFooterBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
            else -> throw RuntimeException("Wrong view type received $viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder.itemViewType) {
            MyItemType.ROW.type -> (holder as PersonViewHolder).bind(getItem(position) as PersonDataUI)
            MyItemType.HEADER.type -> (holder as PersonHeaderViewHolder).bind(getItem(position) as ObjectDataHeaderSample)
            MyItemType.FOOTER.type -> (holder as PersonFooterViewHolder).bind(getItem(position) as ObjectDataFooterSample)
            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")
        }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is PersonDataUI -> MyItemType.ROW.type
            is ObjectDataHeaderSample -> MyItemType.HEADER.type
            is ObjectDataFooterSample -> MyItemType.FOOTER.type
        }
    }
}

class PersonViewHolder(
    private val binding: ItemCustomRecyclerBinding,
    onItemClick: (personDataUI: PersonDataUI, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: PersonDataUI

    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
    }

    fun bind(personDataUI: PersonDataUI) {
        ui = personDataUI
        binding.itemRecyclerViewVersionName.text = "${personDataUI.personFirstName} ${personDataUI.personLastName}"
        binding.itemRecyclerViewVersionCode.text = personDataUI.personGender
        Glide.with(itemView.context)
            .load(personDataUI.personAvatar)
            .placeholder(R.drawable.ic_launcher_background)
            .into(binding.itemRecyclerViewVersionImage)
    }
}

class PersonHeaderViewHolder(
    private val binding: ItemCustomRecyclerHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(objectDataHeaderSample: ObjectDataHeaderSample) {
        binding.itemRecyclerViewHeader.text = objectDataHeaderSample.header
    }
}

class PersonFooterViewHolder(
    private val binding: ItemCustomRecyclerFooterBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(objectDataFooterSample: ObjectDataFooterSample) {
        binding.itemRecyclerViewFooter.text = objectDataFooterSample.footer
    }
}

enum class MyItemType(val type: Int) {
    ROW(0),
    HEADER(1),
    FOOTER(2)
}