package fr.anchorwave.cramezproject.view

import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import fr.anchorwave.cramezproject.databinding.ActivityRecyclerViewBinding
import fr.anchorwave.cramezproject.model.*
import fr.anchorwave.cramezproject.viewmodel.PersonViewModel

class PersonActivity : AppCompatActivity() {

    private lateinit var adapter: PersonAdapter
    private lateinit var binding: ActivityRecyclerViewBinding
    private lateinit var viewModel: PersonViewModel

    private val personListObserver = Observer<List<MyObjectForRecyclerView>> {
        adapter.submitList(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this)[PersonViewModel::class.java]

        // Create the instance of adapter
        adapter = PersonAdapter { item, view ->
            onItemClick(item, view)
        }

        // We define the style
        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        // We set the adapter to recycler view
        binding.recyclerView.adapter = adapter

        binding.addItemButton.setOnClickListener { addRandomPerson() }
        binding.deleteAllItemButton.setOnClickListener { deletePerson() }
    }

    override fun onStart() {
        super.onStart()
        viewModel.personListLiveData.observe(this, personListObserver)
    }

    override fun onStop() {
        super.onStop()
        viewModel.personListLiveData.observe(this, personListObserver)
    }

    private fun addRandomPerson() {
        viewModel.fetchNewPerson()
    }

    private fun deletePerson() {
        viewModel.deleteAllPerson()
    }

    private fun onItemClick(personDataUI: PersonDataUI, view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        Toast.makeText(this, personDataUI.personFirstName, Toast.LENGTH_LONG).show()
    }
}
