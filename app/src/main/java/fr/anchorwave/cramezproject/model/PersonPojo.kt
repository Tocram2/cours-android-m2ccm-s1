package fr.anchorwave.cramezproject.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

sealed class MyObjectForRecyclerView(label : String)

data class ObjectDataHeaderSample(
    val header: String
) : MyObjectForRecyclerView(label = header)

data class ObjectDataFooterSample(
    val footer: String
) : MyObjectForRecyclerView(label = footer)

data class PersonDataUI(
    val personFirstName: String,
    val personLastName: String,
    val personGender: String,
    val personAvatar : String
) : MyObjectForRecyclerView(label = personLastName)

@Entity(tableName = "person_object_table")
data class RoomPersonData(
    @ColumnInfo(name = "first_name")
    val first_name: String,

    @ColumnInfo(name = "last_name")
    val last_name: String,

    @ColumnInfo(name = "gender")
    val gender: String,

    @ColumnInfo(name = "avatar")
    val avatar: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

data class RetrofitPersonData(
    @Expose
    @SerializedName("first_name")
    val first_name: String,

    @Expose
    @SerializedName("last_name")
    val last_name: String,

    @Expose
    @SerializedName("gender")
    val gender: String,

    @Expose
    @SerializedName("avatar")
    val avatar: String
)
