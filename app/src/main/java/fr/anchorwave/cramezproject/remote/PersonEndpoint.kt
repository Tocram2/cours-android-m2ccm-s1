package fr.anchorwave.cramezproject.remote

import fr.anchorwave.cramezproject.model.RetrofitPersonData
import retrofit2.http.GET

interface PersonEndpoint {
    @GET("users")
    suspend fun getRandomPerson() : RetrofitPersonData
}
