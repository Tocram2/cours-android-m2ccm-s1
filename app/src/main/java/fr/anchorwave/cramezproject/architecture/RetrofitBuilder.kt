package fr.anchorwave.cramezproject.architecture

import com.google.gson.GsonBuilder
import fr.anchorwave.cramezproject.remote.PersonEndpoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://random-data-api.com/api/v2/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()

    fun getPersonData(): PersonEndpoint = retrofit.create(PersonEndpoint::class.java)
}