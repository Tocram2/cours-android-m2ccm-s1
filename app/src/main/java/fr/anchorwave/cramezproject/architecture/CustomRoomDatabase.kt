package fr.anchorwave.cramezproject.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.anchorwave.cramezproject.dao.PersonDao
import fr.anchorwave.cramezproject.model.RoomPersonData

@Database(
    entities = [
        RoomPersonData::class
    ],
    version = 2,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}